package main

import (
	"bufio"
	"fmt"
	"net/http"
	"os"
	"time"
)

func helloHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
		currentTime := time.Now()
		fmt.Fprintf(w, currentTime.Format(">15h04"))
	case http.MethodPost:
		fmt.Println("Something went bad")
		if err := req.ParseForm(); err != nil {
			fmt.Println("Something went bad")
			fmt.Fprintln(w, "Something went bad")
			return
		}
		fmt.Println(req.PostForm)

		saveFile, err := os.OpenFile("./save.data", os.O_RDWR|os.O_CREATE, 0755)
		defer saveFile.Close()

		w := bufio.NewWriter(saveFile)
		author := ""
		entry := ""
		if err == nil {
			for key, value := range req.PostForm {
				if author == key {
					author = req.PostForm["author"]
				}
				if entry == key {
					entry = req.PostForm["entry"]
				}
				fmt.Println(key, "=>", value)
				fmt.Fprintf(w, "%s:%d\n", key, value)
			}
		}
		w.Flush()

		fmt.Fprintf(w, "Information received: %v\n", req.PostForm)
	}
}
func main() {
	http.HandleFunc("/add", helloHandler)
	http.HandleFunc("/", helloHandler)
	http.ListenAndServe(":4567", nil)
}
