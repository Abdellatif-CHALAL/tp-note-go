package main

import (
	"bufio"
	"fmt"
	"net/http"
	"time"
)

func helloHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
		currentTime := time.Now()
		fmt.Fprintf(w, currentTime.Format(">15h04"))
	case http.MethodPost:
		fmt.Println("Something went bad")
		if err := req.ParseForm(); err != nil {
			fmt.Println("Something went bad")
			fmt.Fprintln(w, "Something went bad")
			return
		}
		author := ""
		entry := ""
		for key := range req.PostForm {
			if "author" == key {
				author = req.PostForm["author"][0]
			}
			if "entry" == key {
				entry = req.PostForm["entry"][0]
			}
		}
		fmt.Fprintf(w, "%s:%s\n", author, entry)

		wr := bufio.NewWriter(saveFile)
		if err == nil {
			fmt.Fprintf(wr, "%s:%d\n", keptScores[i].pseudo, keptScores[i].score)

		}
		wr.Flush()

		fmt.Fprintf(wf, "Information received: %v\n", req.PostForm)
	}
}
func main() {
	http.HandleFunc("/add", helloHandler)
	http.HandleFunc("/", helloHandler)
	http.ListenAndServe(":4567", nil)
}
