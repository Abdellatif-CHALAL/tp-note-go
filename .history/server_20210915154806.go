package main

import (
	"bufio"
	"fmt"
	"net/http"
	"os"
	"time"
)

func helloHandlerHome(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:

		currentTime := time.Now()
		fmt.Fprintf(w, currentTime.Format(">15h04"))
	case http.MethodPost:
		fmt.Println("Something went bad")
		if err := req.ParseForm(); err != nil {
			fmt.Println("Something went bad")
			fmt.Fprintln(w, "Something went bad")
			return
		}
		author := ""
		entry := ""
		for key := range req.PostForm {
			if "author" == key {
				author = req.PostForm["author"][0]
			}
			if "entry" == key {
				entry = req.PostForm["entry"][0]
			}
		}
		fmt.Fprintf(w, "%s:%s\n", author, entry)

		saveFile, err := os.OpenFile("./save.data", os.O_APPEND|os.O_RDWR|os.O_CREATE, 0755)
		defer saveFile.Close()
		wr := bufio.NewWriter(saveFile)
		if err == nil {
			fmt.Fprintf(wr, "%s:%d\n", author, entry)

		}
		wr.Flush()
	}
}

func helloHandlerEntries(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
		saveData, err := os.ReadFile("./save.data")
		if err == nil {
			fmt.Fprintf(wr, "%s:%d\n", author, entry)
		}

	}
}
func main() {
	http.HandleFunc("/add", helloHandlerHome)
	http.HandleFunc("/", helloHandlerHome)
	http.HandleFunc("/entries", helloHandlerEntries)
	http.ListenAndServe(":4567", nil)
}
